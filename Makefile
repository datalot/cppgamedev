##############################
# MACROS
##############################

# $(1) : Compiler
# $(2) : Generated file
# $(3) : Source file
# $(4) : Dependencies
# $(5) : Compiler flags
#$(eval $(call COMPILE,g++,obj/main.o,src/main.c,main.h,--warn-all --pedantic))
define COMPILE
$(2) : $(3) $(4)
	$(1) --output=$(2) --compile $(3) $(5)
endef

# $(1) : Source to be converted
#$(eval $(call SRC_TO_OBJ,src/main.cpp)) return obj/main.o
define SRC_TO_OBJ
$(patsubst $(SRC_PATH)/%,$(OBJ_PATH)/%,$(patsubst $(SRC_PATH)/%.cpp,$(OBJ_PATH)/%.o,$(patsubst $(SRC_PATH)/%.c,$(OBJ_PATH)/%.o,$(1))))
endef

# $(1) : Source to be converted
#$(eval $(call SRC_TO_OBJ,src/main.cpp)) return src/main.h
define SRC_TO_H
$(patsubst $(SRC_PATH)/%.cpp,$(SRC_PATH)/%.hpp,$(patsubst $(SRC_PATH)/%.c,$(SRC_PATH)/%.h,$(1)))
endef

##############################
# CONFIG
##############################

APP				:= game
NULL			:=
CPP_COMPILER	:= g++
C_COMPILER		:= gcc
CPP_FLAGS		:= --warn-all --pedantic
C_FLAGS			:= $(CPP_FLAGS)
MKDIR			:= mkdir --parents # Substitute the 
SRC_PATH		:= src
OBJ_PATH		:= obj
LIBS			:= -lX11 #-lXext

# <a> Following code stores and recreates the structure of dirs and files
#     that will be used and generated in compilation time. 
ALL_SUBDIRS		:= $(shell find $(SRC_PATH)/ -type d)				# Store the directory structure of SRC_PATH
ALL_CPP_FILES	:= $(shell find $(SRC_PATH)/ -type f -iname *.cpp)	# And store all .cpp files in the whole SRC_PATH structure
ALL_C_FILES		:= $(shell find $(SRC_PATH)/ -type f -iname *.c)	# Same for .c files
ALL_OBJ_SUBDIRS	:= $(call SRC_TO_OBJ,$(ALL_SUBDIRS))				# Now, replicate that structure under OBJ_PATH instead of SRC_PATH
ALL_C_OBJS		:= $(call SRC_TO_OBJ,$(ALL_C_FILES))				# Store all those .c files but as .o instead 
ALL_CPP_OBJS	:= $(call SRC_TO_OBJ,$(ALL_CPP_FILES))				# And do the same for .cpp files
ALL_OBJ_FILES	:= $(ALL_C_OBJS) $(ALL_CPP_OBJS)
# <a/>

##############################
# COMPILE
##############################

# <a> Compile all .c and .cpp files then link them and generate the APP app.
$(APP) : $(ALL_OBJ_SUBDIRS) $(ALL_OBJ_FILES)
	$(CPP_COMPILER) --output=$(APP) $(ALL_OBJ_FILES) $(LIBS)

# foreach variable,list,do
$(foreach F,$(ALL_CPP_FILES),$(eval $(call COMPILE,$(CPP_COMPILER),$(call SRC_TO_OBJ,$(F)),$(F),$(call SRC_TO_H,$(F)),$(CPP_FLAGS))))
$(foreach F,$(ALL_C_FILES),$(eval $(call COMPILE,$(C_COMPILER),$(call SRC_TO_OBJ,$(F)),$(F),$(call SRC_TO_H,$(F)),$(C_FLAGS))))
# <a/>

##############################
# PHONYS
##############################

.PHONY: show clean
show:
	$(info $(ALL_SUBDIRS))
	$(info $(ALL_OBJ_SUBDIRS))
	$(info $(ALL_CPP_FILES))
	$(info $(ALL_CPP_OBJS))
	$(info $(ALL_C_FILES))
	$(info $(ALL_C_OBJS))
clean:
	rm -rf $(ALL_OBJ_FILES) $(ALL_OBJ_SUBDIRS)
$(ALL_OBJ_SUBDIRS):
	$(MKDIR) $(ALL_OBJ_SUBDIRS)
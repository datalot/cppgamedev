#pragma once
#include <cstdint>
#include <memory>
//#include "EntityManager_t.hpp"

namespace datalot {

//0xALPHA,RED,GREEN,BLUE
static constexpr uint32_t R       {0x00FF0000}; /*RED COLOR*/
static constexpr uint32_t G       {0x0000FF00}; /*GREEN COLOR*/
static constexpr uint32_t B       {0x000000FF}; /*BLUE COLOR*/
static constexpr uint32_t BLACK   {0xFF000000};
static constexpr uint32_t WHITE   {0xFFFFFFFF};

static constexpr uint32_t defaultSprite[8*8] = {
	G,G,G,G,G,G,G,G,
	G,B,R,R,R,R,B,G,
	G,B,R,G,G,G,B,G,
	G,B,B,R,G,G,B,G,
	G,B,B,B,R,G,B,G,
	G,B,B,B,B,R,B,G,
	G,B,R,R,R,R,B,G,
	G,G,G,G,G,G,G,G
};

struct EntityManager_t;
struct RenderEngine_t {
    private:
    	const uint32_t SCREEN_WIDTH  {0};
    	const uint32_t SCREEN_HEIGHT {0};
		//RAII Pattern:
		std::unique_ptr<uint32_t[]> frameBuffer { nullptr }; // auto frameBuffer = std::make_unique<uint32_t[]>(640*360);
		EntityManager_t & ENTITY_MANAGER;
	public:
    	explicit RenderEngine_t(uint32_t screenWidth, uint32_t screenHeight, EntityManager_t & entityManager);
    	~RenderEngine_t();
    	bool update() const;
		void drawAllEntities() const;
		void drawBackground() const;
};
    
}
#pragma once
#include <cstdint>
#include <vector>

namespace datalot {

struct Entity_t{
    uint32_t xPosition {0};
    uint32_t yPosition {0};
    const uint32_t WIDTH {0};
    const uint32_t HEIGHT {0};
    std::vector<uint32_t> sprite {}; /*Stores the pixels of the entity.*/

    explicit Entity_t(uint32_t w, uint32_t h);
};

struct EntityManager_t {
private:
    static constexpr uint32_t MAXIMUM_ENTITIES {1000};
    std::vector<Entity_t> entities {};
public:
	explicit EntityManager_t();

	void createEntity(const uint32_t WIDTH, const uint32_t HEIGHT, uint32_t xPosition, uint32_t yPosition, uint32_t color);
	const std::vector<Entity_t> & getEntities() const;
};

}
#include <iostream>
#include "main.hpp"
#include "RenderEngine_t.hpp"
#include "EntityManager_t.hpp"

extern "C" {
	#include "tinyptc/tinyptc.h"
}

int main(int argc, char const *argv[]) {

	constexpr uint32_t SCREEN_WIDTH {640};
	constexpr uint32_t SCREEN_HEIGHT {360};

	try {
		datalot::EntityManager_t entityManager;
		entityManager.createEntity(32, 64, 64, 32, 0xFFFFFFFF);
		const datalot::RenderEngine_t game{SCREEN_WIDTH, SCREEN_HEIGHT, entityManager};
		while (game.update());
	} catch(...) {
		std::cout << "Hubo una excepción no resuelta!" << '\n';
	}

	return 0;
}
#include "RenderEngine_t.hpp"
#include "EntityManager_t.hpp"
#include <iostream>
#include <algorithm>

extern "C" {
	#include "tinyptc/tinyptc.h"
}

using namespace datalot;

/*Starts a whole instance of a window and */
RenderEngine_t::RenderEngine_t(uint32_t screen_width, uint32_t screen_height, EntityManager_t& entityManager)
:	SCREEN_WIDTH{screen_width}
,	SCREEN_HEIGHT{screen_height}
,	frameBuffer{std::make_unique<uint32_t[]>(screen_width * screen_height)}
,	ENTITY_MANAGER{entityManager}
{
	ptc_open("DATALOT",screen_width,screen_height);
}

/*Calls the ptc_close() method*/
RenderEngine_t::~RenderEngine_t() {
	ptc_close();
}


void RenderEngine_t::drawAllEntities() const {
	auto & entities {ENTITY_MANAGER.getEntities()};

	for (auto & entity : entities) {									// For each entity,
		auto screen = frameBuffer.get();								// Set the draw location to 0,0,
		screen += entity.yPosition * SCREEN_WIDTH + entity.xPosition;	// Then point to the draw location to the entity position.
		auto spriteIterator = entity.sprite.begin();					// Let's store this for practicality.

		for (uint32_t Y = 0; Y < entity.HEIGHT; ++Y) {							// Now, for every sprite's row,
			std::copy(spriteIterator, spriteIterator + entity.WIDTH, screen);	// Copy the whole row to the buffer,
			spriteIterator += entity.WIDTH;										// Jump to the next sprite's row,
			screen += SCREEN_WIDTH;												// And jump to the next screen row. We're done.
		}
	}

	// // - Following code works with pointer arithmetics, assuming
	// // that each pointer location is equivalent to pixel location.
	// // - Remember that we are using 4-bytes-unsigned-ints (u_int32_t) as
	// // pointer type, so arithmetics will be done 4-bytes at time.
	// const uint32_t *sprite_ptr = datalot::sprite; /*Pointer is modifiable. Address value is not. See East/West const.*/
	// screen_ptr = screen_ptr; // Resets the pointer location to the first screen pixel
	// sprite_ptr = datalot::sprite; // Resets the pointer location to the first sprite pixel
	// for (uint32_t x = 0; x < 8; ++x) {
	// 	for (uint32_t y = 0; y < 8; ++y) {
	// 		*screen_ptr = *sprite_ptr;  // Assigns pointed sprite pixel to pointed screen pixel (we start with the first)
	// 		++screen_ptr;               // Switch to next screen pixel
	// 		++sprite_ptr;               // Switch to next sprite pixel
	// 	}
	// 	/*Jump to the next line of pixels, but as we have left in the 8
	// 	screen pixel for this sprite, we should go to the first pixel again (-8).*/
	// 	screen_ptr += 640 - 8;
	// }
}

void RenderEngine_t::drawBackground() const {
	auto screen = frameBuffer.get();
	//const uint32_t size = SCREEN_WIDTH * SCREEN_HEIGHT;
	//std::fill(screen, screen + size, datalot::R); // Paint a red background 

	for (uint32_t i = 0; i < (SCREEN_WIDTH*SCREEN_HEIGHT); i++)
		screen[i] = rand() * 0xFFFFFFFF; // Generate noise background
}

bool RenderEngine_t::update() const {
	drawBackground();
	drawAllEntities();
	ptc_update(frameBuffer.get()); // Prints the buffer in the screen

	return !ptc_process_events();
}
#include "EntityManager_t.hpp"

using namespace datalot;

Entity_t::Entity_t(const uint32_t WIDTH, const uint32_t HEIGHT)
:   WIDTH{WIDTH}
,   HEIGHT{HEIGHT}
{
    sprite.resize(WIDTH*HEIGHT);
}

EntityManager_t::EntityManager_t() {
    entities.reserve(MAXIMUM_ENTITIES);
}

void EntityManager_t::createEntity(const uint32_t WIDTH, const uint32_t HEIGHT, uint32_t xPosition, uint32_t yPosition, uint32_t color) {
    /*emplace_back() takes all the arguments of the Entity_t's constructor,
    This way, we avoid duplication on heap memory doing:
    Entity_t entity{16,16};
    entities.push_back(entity);
    */
    auto & entity = entities.emplace_back(WIDTH, HEIGHT);
    entity.xPosition = xPosition;
    entity.yPosition = yPosition;
    std::fill(entity.sprite.begin(), entity.sprite.end(), color);
}

const std::vector<Entity_t> & EntityManager_t::getEntities() const {
    return entities;
}
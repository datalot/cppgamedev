### About

This project is a product of my follow-up to the course "[GameDev C++ 2020: Programación de Videojuegos desde 0 para PC](https://www.youtube.com/playlist?list=PLmxqg54iaXrhTqZxylLPo0nov0OoyJqiS)" from [Dr. Francisco Gallego](https://twitter.com/FranGallegoBR) also known as ([Profesor Retroman on YouTube](https://www.youtube.com/@ProfesorRetroman)).

### Motive

The point of this repository is to keep track of changes, while also maintaining my own coding style throughout the project development. Lots of explainful, detailed, step-by-step comments have been added to the codebase to preserve the thoughts behind some decisions.

### Side Notes

I've not created the repository since the first day, so a lot of deleted/changed information has been lost, but can be found throughout the first 17 chapters of the course playlist. By the way, to the date of creating this repository, I've completed the _Chapter 17: "C++ : Function objects, operator() y lambdas"_

### Warnings

This project contains the TinyPTC Library source code. No license was provided with it so be careful when forking this project.